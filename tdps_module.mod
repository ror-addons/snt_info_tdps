<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">    
  <UiMod name="SNT Info [TDPS]" version="1.0.1" date="2/24/2010" >        
    <Author name="Gridgendal" />        
    <Description text="SNT INFO Support For Tortall's DPS Meter" />    
    <VersionSettings gameVersion="1.9.9" windowsVersion="1.0" savedVariablesVersion="1.0.1" />          
    <SavedVariables>            
      <SavedVariable name="snt_info.tdps_module.Settings" />        
    </SavedVariables>        
    <Dependencies>            
      <Dependency name="SNT_INFO" optional="false" forceEnable="true" />            
      <Dependency name="Tortall_DPS" optional="false" forceEnable="true" />        
    </Dependencies>        
    <Files>            
      <File name="tdps_module.lua" />        
    </Files>        
    <OnInitialize>            
      <CallFunction name="snt_info.tdps_module.entry_point" />        
    </OnInitialize>        
    <OnShutdown>            
      <CallFunction name="snt_info.tdps_module.Shutdown" />        
    </OnShutdown>    
  </UiMod>
</ModuleFile>
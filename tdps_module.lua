snt_info.tdps_module = {}

local mode = 0
local DPS = 0
local HPS = 0

local init = {
        mode = 0,
        LastDPS = 0,
        LastHPS = 0,
    }

if not snt_info.tdps_module.Settings then
    snt_info.tdps_module.Settings = init
else
    for k,v in pairs(init) do
        if not snt_info.tdps_module.Settings[k] then snt_info.tdps_module.Settings[k] = v end
    end
end

function snt_info.tdps_module.Callback(damage, healing, time)
    if mode == 0 then
        -- grab the per second from tdps
	   local ps = damage.Dealt.Total.Amount / time
       if ( time == 0.0 ) then ps = DPS end
	   -- set the title
	   i_tdps:set_text(string.format("%.2f",ps))
       i_tdps:set_text_col(200,0,0)
    else
        -- grab the per second from tdps
	   local ps = healing.Dealt.Total.Amount / time
       if ( time == 0.0 ) then ps = HPS end
	   -- set the title
	   i_tdps:set_text(string.format("%.2f",ps))
       i_tdps:set_text_col(0,200,0)
    end
    -- save HPS/DPS data
    if ( (damage.Dealt.Total.Amount / time) > 0 ) then DPS = damage.Dealt.Total.Amount / time end
    if ( (healing.Dealt.Total.Amount / time) > 0 )then HPS = healing.Dealt.Total.Amount / time end
end

function snt_info.tdps_module.rclick()
    -- Toggles between DPS and HPS mode
    if mode == 0 then
        mode = 1
        i_tdps:set_text(string.format("%.2f",HPS))
        i_tdps:set_text_col(0,200,0)
    else
        mode = 0
        i_tdps:set_text(string.format("%.2f",DPS))
        i_tdps:set_text_col(200,0,0)
    end
    snt_info.tdps_module.SaveSettings()
end

function snt_info.tdps_module.SaveSettings()
    snt_info.tdps_module.Settings.mode = mode
    snt_info.tdps_module.Settings.LastHPS = HPS
    snt_info.tdps_module.Settings.LastDPS = DPS
end

function snt_info.tdps_module.LoadSettings()
    mode = snt_info.tdps_module.Settings.mode
    DPS = snt_info.tdps_module.Settings.LastDPS
    HPS = snt_info.tdps_module.Settings.LastHPS
end

function snt_info.tdps_module.lclick() TortallDPSMeter.Toggle() end

function snt_info.tdps_module.Shutdown() snt_info.tdps_module.SaveSettings() end

--------------------Entry Point------------------------------
function snt_info.tdps_module.entry_point()
    TortallDPSCore.Register("tdps_module", snt_info.tdps_module.Callback)
    i_tdps = snt_info.informer:new("i_tdps",6,14,nil,"snt_info.tdps_module.lclick","snt_info.tdps_module.rclick")
    snt_info.tdps_module.LoadSettings()
    if mode == 0 then
       i_tdps:set_text(string.format("%.2f",DPS))
       i_tdps:set_text_col(200,0,0)
    else
       i_tdps:set_text(string.format("%.2f",HPS))
       i_tdps:set_text_col(0,200,0)
    end
    WindowSetShowing("TortallDPSToggle", false)
end